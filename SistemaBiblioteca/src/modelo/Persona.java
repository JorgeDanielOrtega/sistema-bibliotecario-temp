/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 * Clase Persona
 *
 * @author daniel
 */
public class Persona {

    /**
     * Id de la persona
     */
    private Integer id;
    /**
     * Nombre de la persona
     */
    private String nombres;
    /**
     * Apellidos de la persona
     */
    private String apellidos;
    /**
     * Cédula de la persona
     */
    private String cedula;
    /**
     * Estado de la persona
     */
    private Boolean estado;
    /**
     * Rol de la persona
     */
    private Rol rol;

    /**
     * Constructor vacio por defecto
     */
    public Persona() {
    }

    public Persona(String nombres, String apellidos, String cedula) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.cedula = cedula;
    }

    public Persona(Integer id, String nombres, String apellidos, String cedula) {
        this.id = id;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.cedula = cedula;
    }

    /**
     * Constructor que crea un nueo objeto de la clase Persona
     *
     * @param id Id de la persona
     * @param nombres Nombres de la persona
     * @param apellidos Apellidos de la persona
     * @param cedula Cédula de la persona
     * @param estado Estado de la persona
     * @param rol Rol de la persona
     */
    public Persona(Integer id, String nombres, String apellidos, String cedula, Boolean estado, Rol rol) {
        this.id = id;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.cedula = cedula;
        this.estado = estado;
        this.rol = rol;
    }

    /**
     * Presenta en un formato aceptable a la persona
     *
     * @return Retorna el formato de la persona
     */
    public String presentar() {
        StringBuilder formato = new StringBuilder();
        formato.append("\t" + "Nombres: " + nombres + "\n");
        formato.append("\t" + "Apellidos: " + apellidos + "\n");
        formato.append("\t" + "Cédula: " + cedula + "\n");
        formato.append("\t" + "Estado: " + estado + "\n");
        formato.append("\t" + "Rol: " + rol.getNombre() + "\n");

        return formato.toString();
    }
    
      public String presentarP() {
        StringBuilder formato = new StringBuilder();
        formato.append("\t" + "Nombres: " + nombres + "\n");
        formato.append("\t" + "Apellidos: " + apellidos + "\n");
        formato.append("\t" + "Cédula: " + cedula + "\n");
        return formato.toString();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    @Override
    public String toString() {
        return "Persona{" + "id=" + id + ", nombres=" + nombres + ", apellidos=" + apellidos + ", cedula=" + cedula + ", estado=" + estado + ", rol=" + rol + '}';
    }

}
