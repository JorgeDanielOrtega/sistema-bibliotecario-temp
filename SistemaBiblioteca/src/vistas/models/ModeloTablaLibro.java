/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vistas.models;

import vistas.models.*;
import controlador.listas.ListaEnlazada;
import controlador.listas.NodoLista;
import controlador.listas.excepciones.ListIsVoidException;
import controlador.listas.excepciones.ListOutLimitException;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import modelo.Libro;

/**
 *
 * @author Usuario
 */
public class ModeloTablaLibro extends AbstractTableModel{
    ListaEnlazada<Libro> listaLibro=new ListaEnlazada<>();
    
    public ListaEnlazada<Libro> getListaLibro() {
        return listaLibro;
    }

    public void setListaLibro(ListaEnlazada<Libro> listaLibro) {
        this.listaLibro = listaLibro;
    }
    

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public int getRowCount() {
        return listaLibro.getSize();
    }

    @Override
    public String getColumnName(int i) {
        
        switch(i) {
            case 0: return "ID";
            case 1: return "Titulo";
            case 2: return "Autor";
            case 3: return "Lugar Publicacion";
            case 4: return "Idioma";
           
            default: return null;
        }
    }

    @Override
    public Object getValueAt(int i, int i1) {
       
        try {
            Libro e= listaLibro.obtener(i);
            switch(i1) {
                case 0: return (e != null) ? e.getId() : "NO DEFINIDO";
                case 1: return (e != null) ? e.getTitulo(): "NO DEFINIDO";
                case 2: return (e != null) ? e.getAutor() : "NO DEFINIDO";
                case 3: return (e != null) ? e.getLugarPublicacion() : "NO DEFINIDO";
                case 4: return (e != null) ? e.getIdioma() : "NO DEFINIDO";
                default: return null;
            }
            
        } catch (ListIsVoidException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        } catch (ListOutLimitException ex) {
JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        return null;
    }
 
    /**
     * selecciona un elemento de la tabla, y muestra el id de este en un Jtextfield
     * @param tablaLibro tabla donde se muestran los elementos
     * @param id donde se mostrara el el id
     */
    public void SeleccionarLibro(JTable tablaLibro, JTextField id){

        try {
            int fila=tablaLibro.getSelectedRow();
            if (fila>=0) {
                id.setText(tablaLibro.getValueAt(fila, 0).toString());             
            }else{
                JOptionPane.showMessageDialog(null, "fila mal seleccionada");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Error: " +e.toString());
        }
    }
    
     
}
