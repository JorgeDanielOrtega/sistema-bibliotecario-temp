package vistas.models;

import controlador.listas.ListaEnlazada;
import javax.swing.table.AbstractTableModel;
import modelo.Cuenta;

/**
 *
 * @author Leo
 */
public class TablaCuenta extends AbstractTableModel {

    private ListaEnlazada<Cuenta> listacuenta;

    public ListaEnlazada<Cuenta> getListCuenta() {
        return listacuenta;
    }

    public void setListCuenta(ListaEnlazada<Cuenta> listacuenta) {
        this.listacuenta = listacuenta;
    }

    @Override
    public int getRowCount() {
        return listacuenta.getSize();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override

    public String getColumnName(int columna) {
        switch (columna) {
            case 0:
                return "ID";
            case 1:
                return "Usuario";
            case 2:
                return "Clave";
            case 3:
                return "Estado";

            default:
                return null;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        try {
            Cuenta C = listacuenta.obtener(rowIndex);
            switch (columnIndex) {
                case 0:
                    return(C!= null) ? C.getId() : "Sin definir"; 
                case 1:
                    return (C != null) ? C.getUsuario() : "Sin Definir";
                case 2:
                    return (C != null) ? "-Oculta-" : "Sin Definir";
                case 3:
                    return (C != null) ? C.getEstaActiva() : "Sin Definir";

                default:
                    
            }
        } catch (Exception e) {
            System.out.println("Error en tabla");
        }
        return null;
    }

}
