
package vistas;

import controlador.ControladorRol;
import controlador.RegistroRol;
import javax.swing.JOptionPane;

import util.Utilidades;
import vistas.models.TablaRol;

/**
 *
 * @author Leo117
 */
public class FrmRol extends javax.swing.JDialog {

    private TablaRol tabla = new TablaRol();
    ControladorRol controlRol = new ControladorRol();
    
    public FrmRol(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        cargarDatosRol();
        btonSelec.setEnabled(false);
        controlRol = (ControladorRol) Utilidades.cargarJson(ControladorRol.class, ControladorRol.NOMBRE_ARCHIVO);
        cargarTabla();
    }
    private void cargarDatosRol(){
        controlRol = (ControladorRol) Utilidades.cargarJson(ControladorRol.class, ControladorRol.NOMBRE_ARCHIVO);
    }
    /**
     * Metodo para reiniciar los datos mostrados en la tabla 
     */
    private void refresh(){
        cargarDatosRol();
        cargarTabla();
        btonSelec.setEnabled(false);
    }
    /**
     * Metodo para mostrar los roles disponibles en la tabla
     */
    private void cargarTabla(){
        tabla.setListRol(controlRol.getRolList());
        tablaRol.setModel(tabla);
        tabla.fireTableDataChanged();
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaRol = new javax.swing.JTable();
        btonRegistro = new javax.swing.JButton();
        btonSelec = new javax.swing.JButton();
        btonNuevo = new javax.swing.JButton();
        btonRefrescar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        tablaRol.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "ID", "Nombre", "Detalle"
            }
        ));
        tablaRol.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaRolMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablaRol);

        btonRegistro.setText("Registro");
        btonRegistro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btonRegistroActionPerformed(evt);
            }
        });

        btonSelec.setText("Seleccionar");
        btonSelec.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btonSelecActionPerformed(evt);
            }
        });

        btonNuevo.setText("Nuevo Rol");
        btonNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btonNuevoActionPerformed(evt);
            }
        });

        btonRefrescar.setText("Refrescar");
        btonRefrescar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btonRefrescarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(130, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(btonNuevo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btonSelec, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(54, 54, 54)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btonRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btonRefrescar))
                .addGap(124, 124, 124))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btonRegistro)
                    .addComponent(btonSelec))
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btonNuevo)
                    .addComponent(btonRefrescar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(79, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(20, 20, 20))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(16, 16, 16))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    /**
     * Boton para generar el registro en formato PDF  de los roles existentes
     * @param evt evento del boton
     */
    private void btonRegistroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btonRegistroActionPerformed
        // TODO add your handling code here:
        try {
            RegistroRol registroRol = new RegistroRol(controlRol.getRolList(), "roles");
            registroRol.crearRegistro("Registro de Roles");
            JOptionPane.showMessageDialog(this, "Registro de Roles creado con exito", "Confirmacion",JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
        }
    }//GEN-LAST:event_btonRegistroActionPerformed
    /**
     * Boton para cargar los datos del rol que se ha elegido de la tabla y mostrarlos en otra vista
     * @param evt 
     */
    private void btonSelecActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btonSelecActionPerformed
        // TODO add your handling code here:
        
        try {
            ModificarRol modificar = new ModificarRol(null, true);
            modificar.setControlRol(controlRol);
            modificar.asignarRol(tablaRol.getSelectedRow());
            modificar.setVisible(true);
            cargarTabla();
            Utilidades.guardarJson(controlRol, "roles");
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }//GEN-LAST:event_btonSelecActionPerformed
    /**
     * Evento para desbloquear el boton de Seleccion despues de haber elegido un rol de la tabla
     * @param evt Seleccion de un rol mostrado en la tabla
     */
    private void tablaRolMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaRolMouseClicked
        // TODO add your handling code here:
        btonSelec.setEnabled(true);
    }//GEN-LAST:event_tablaRolMouseClicked
    /**
     * boton para mostrar la creacion de un nuevo rol
     * @param evt  evento del boton 
     */
    private void btonNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btonNuevoActionPerformed
        // TODO add your handling code here:
        try{
            ModificarRol md = new ModificarRol(null,true);
            md.desbloquear();
            md.setVisible(true);
            
        }catch(Exception e){
            
        }
    }//GEN-LAST:event_btonNuevoActionPerformed
    /**
     * actualizacion de los datos de la tabla en caso de una modificacion 
     * @param evt 
     */
    private void btonRefrescarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btonRefrescarActionPerformed
        // TODO add your handling code here:
        refresh();
    }//GEN-LAST:event_btonRefrescarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmRol.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmRol.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmRol.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmRol.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmRol dialog = new FrmRol(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btonNuevo;
    private javax.swing.JButton btonRefrescar;
    private javax.swing.JButton btonRegistro;
    private javax.swing.JButton btonSelec;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaRol;
    // End of variables declaration//GEN-END:variables
}
