/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas.exceptions;

import vistas.exceptions.*;

/**
 * Clase excepción para el elemento no encontrado
 *
 * @author daniel
 */
public class ElementoNoEncontradoExcepcion extends Exception {

    /**
     * Constructor vacio que contiene el mesaje por defecto "El elemento no
     * existe"
     */
    public ElementoNoEncontradoExcepcion() {
        super("El elemento no existe");
    }

    /**
     * Constructor que muestra el mensaje que se le pase como parametro
     *
     * @param msg Mensaje que se mostrará en la excepción
     */
    public ElementoNoEncontradoExcepcion(String msg) {
        super(msg);
    }

}
