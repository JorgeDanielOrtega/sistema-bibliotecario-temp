/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas.exceptions;

import vistas.exceptions.*;

/**
 * Clase excepción para los campos vacios
 *
 * @author daniel
 */
public class CamposVaciosExcepcion extends Exception {

    /**
     * Constructor vacio que contiene el mesaje por defecto "Los campos estan
     * vacios"
     */
    public CamposVaciosExcepcion() {
        super("Los campos estan vacios");
    }

    /**
     * Constructor que muestra el mensaje que se le pase como parametro
     *
     * @param msg Mensaje que se mostrará en la excepción
     */
    public CamposVaciosExcepcion(String msg) {
        super(msg);
    }

}
