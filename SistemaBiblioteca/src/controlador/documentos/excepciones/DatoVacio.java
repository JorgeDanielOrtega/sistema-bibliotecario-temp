/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.documentos.excepciones;

/**
 *
 * @author Usuario
 */
public class DatoVacio extends Exception{

    public DatoVacio() {
        super("Complete todos los campos");
    }

    public DatoVacio(String message) {
        super(message);
    }
    
}
