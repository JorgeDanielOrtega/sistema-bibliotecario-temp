/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.documentos.excepciones;

/**
 *
 * @author Usuario
 */
public class FechaNoValida extends Exception{

    public FechaNoValida() {
    super("Fecha no valida\n Formato: dd/MM/yyyy");
    }

    public FechaNoValida(String message) {
        super(message);
    }
    
    
}
