package controlador;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import controlador.listas.ListaEnlazada;
import controlador.listas.excepciones.ListIsVoidException;
import controlador.listas.excepciones.ListOutLimitException;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import modelo.Rol;

/**
 *
 * @author Leo117
 */
public class RegistroRol extends Registro {

    ListaEnlazada<Rol> rolList;

    /**
     * Constructor con un parametro
     *
     * @param nombreArchivo Nombre de como se quiere que se guarde el archivo
     * pdf
     */
    public RegistroRol(String nombreArchivo) {
        super(nombreArchivo);
    }

    /**
     * Constructor con dos parametros
     *
     * @param rolList Lista de roles a representar en el pdf
     * @param nombreArchivo Nombre de como se quiere que se guarde el archivo
     * pdf
     */
    public RegistroRol(ListaEnlazada<Rol> rolList, String nombreArchivo) {
        super(nombreArchivo);
        this.rolList = rolList;
    }

    /**
     * Crea en registro en pdf
     *
     * @param titulo Titulo que contendra el pdf
     * @return Retorna true si todo a salido bien
     * @throws FileNotFoundException Excepcion que se lanza cuando el archivo no
     * ha sido encontrado
     * @throws DocumentException Excepcion que se lanza cuando ocurre una
     * excepcion de documento
     * @throws ListIsVoidException Excepcion que se lanza cuando la lista esta
     * vacia
     * @throws ListOutLimitException Excepcion que se lanza cuando la posicion
     * supera los limites de la lista
     */
    @Override
    public Boolean crearRegistro(String titulo) throws FileNotFoundException, DocumentException, ListIsVoidException, ListOutLimitException {
        Document doc = new Document();
        PdfWriter.getInstance(doc, new FileOutputStream(fullpath));
        doc.open();

        Font fuenteTitulo = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
        Font fuenteDescripcion = new Font(Font.FontFamily.COURIER, 12, Font.ITALIC);

        Paragraph tituloText = new Paragraph(titulo, fuenteTitulo);
        tituloText.setAlignment(com.itextpdf.text.Element.ALIGN_CENTER);

        Paragraph descripcion = new Paragraph("\n" + "Creado el: " + getFecha() + "\n\n", fuenteDescripcion);

        List l = new List(false);
        for (int i = 0; i < rolList.getSize(); i++) {
            Paragraph item = new Paragraph(rolList.obtener(i).presentar());
            l.add(item.toString());
        }
        doc.add(tituloText);
        doc.add(descripcion);
        doc.add(l);
        doc.close();
        
        System.out.println("pdf creado");
        return true;
    }

    public ListaEnlazada<Rol> getRolList() {
        return rolList;
    }

    public void setRolList(ListaEnlazada<Rol> rolList) {
        this.rolList = rolList;
    }
}
