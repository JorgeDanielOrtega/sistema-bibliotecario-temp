/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import controlador.listas.ListaEnlazada;
import controlador.listas.excepciones.ListIsVoidException;
import controlador.listas.excepciones.ListOutLimitException;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import modelo.Prestamo;

/**
 *
 * @author cobos
 */
public class RegistroPrestamo extends Registro{

     ListaEnlazada<Prestamo> prestamoList;

    /**
     * Constructor con un parametro
     *
     * @param nombreArchivo Nombre de como se quiere que se guarde el archivo
     * pdf
     */
    public RegistroPrestamo(String nombreArchivo) {
        super(nombreArchivo);
    }

    /**
     * Constructor con dos parametros
     *
     * @param prestamoList Lista de empleados a representar en el pdf
     * @param nombreArchivo Nombre de como se quiere que se guarde el archivo
     * pdf
     */
    public RegistroPrestamo(ListaEnlazada<Prestamo> prestamoList, String nombreArchivo) {
        super(nombreArchivo);
        this.prestamoList = prestamoList;
    }

    /**
     * Crea en registro en pdf
     *
     * @param titulo Titulo que contendra el pdf
     * @return Retorna true si todo a saliido bien
     * @throws FileNotFoundException Excepcion que se lanza cuando el archivo no
     * ha sido encontrado
     * @throws DocumentException Excepcion que se lanza cuando ocurre una
     * excepcion de documento
     * @throws ListIsVoidException Excepcion que se lanza cuando la lista esta
     * vacia
     * @throws ListOutLimitException Excepcion que se lanza cuando la posicion
     * supera los limites de la lista
     */
    @Override
    public Boolean crearRegistro(String titulo) throws FileNotFoundException, DocumentException, ListIsVoidException, ListOutLimitException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(fullpath));
        document.open();

        Font fuenteTitulo = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
        Font fuenteDescripcion = new Font(Font.FontFamily.COURIER, 12, Font.ITALIC);

        Paragraph tituloText = new Paragraph(titulo, fuenteTitulo);
        tituloText.setAlignment(com.itextpdf.text.Element.ALIGN_CENTER);

        Paragraph descripcion = new Paragraph("\n" + "Creado el: " + getFecha() + "\n\n", fuenteDescripcion);

        List l = new List(false);
        for (int i = 0; i < prestamoList.getSize(); i++) {
            Paragraph item = new Paragraph(prestamoList.obtener(i).presentar());
            l.add(item.toString());
        }

        document.add(tituloText);
        document.add(descripcion);
        document.add(l);
        document.close();

        System.out.println("pdf creado");
        return true;
    }

    public ListaEnlazada<Prestamo> getEmpleadoList() {
        return prestamoList;
    }

    public void setEmpleadoList(ListaEnlazada<Prestamo> prestamoList) {
        this.prestamoList = prestamoList;
    }

}
