package controlador.listas;

/**
 * Clase nodo de la lista
 *
 * @author daniel
 */
public class NodoLista<E> {

    /**
     *
     * Dato del nodo, por defecto es null
     */
    private E dato;
    /**
     * Nodo siguiente, por defecto es null
     */
    private NodoLista<E> siguiente;
    /**
     * Nodo anterior, por defecto es null
     */
    private NodoLista<E> anterior;

    /**
     * Constructos sin parámetros que inicializa a null los atrbiutos de la
     * clase
     */
    public NodoLista() {
        this.dato = null;
        this.siguiente = null;
        this.anterior = null;
    }

    /**
     * Constructor que inicializa el atributo dato y siguente
     *
     * @param dato Dato del nodo
     * @param siguiente Nodo siguiente
     */
    public NodoLista(E dato, NodoLista<E> siguiente) {
        this.dato = dato;
        this.siguiente = siguiente;
    }

    /**
     * Constructor que inicializa el atributo dato, siguente y anterior
     *
     * @param dato Dato del nodo
     * @param siguiente Nodo siguiente
     * @param anterior Nodo anterior
     */
    public NodoLista(E dato, NodoLista<E> siguiente, NodoLista<E> anterior) {
        this.dato = dato;
        this.siguiente = siguiente;
        this.anterior = anterior;
    }

    public E getDato() {
        return dato;
    }

    public void setDato(E dato) {
        this.dato = dato;
    }

    public NodoLista<E> getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(NodoLista<E> siguiente) {
        this.siguiente = siguiente;
    }

    public NodoLista<E> getAnterior() {
        return anterior;
    }

    public void setAnterior(NodoLista<E> anterior) {
        this.anterior = anterior;
    }

}
