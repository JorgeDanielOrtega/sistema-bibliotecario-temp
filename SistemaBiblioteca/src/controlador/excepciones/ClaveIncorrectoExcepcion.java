/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.excepciones;

/**
 * Clase excepción para la clave incorrecta
 *
 * @author daniel
 */
public class ClaveIncorrectoExcepcion extends Exception {

    /**
     * Constructor vacio que contiene el mesaje por defecto, mostrando los
     * intentos restantes
     * @param intentos intentos
     */
    public ClaveIncorrectoExcepcion(Integer intentos) {
        super("La clave es incorrecta, te queda " + intentos + " intento(s)");
    }

    /**
     * Constructor que muestra el mensaje que se le pase como parametro
     *
     * @param msg Mensaje que se mostrará en la excepción
     */
    public ClaveIncorrectoExcepcion(String msg) {
        super(msg);
    }

}
