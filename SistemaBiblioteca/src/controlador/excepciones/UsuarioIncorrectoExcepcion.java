/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.excepciones;

/**
 * Clase excepción para el usuario incorrecto
 *
 * @author daniel
 */
public class UsuarioIncorrectoExcepcion extends Exception {

    /**
     * Constructor vacio que contiene el mesaje por defecto "El usuario es
     * incorrecto"
     */
    public UsuarioIncorrectoExcepcion() {
        super("El usuario es incorrecto");
    }

    /**
     * Constructor que muestra el mensaje que se le pase como parametro
     *
     * @param msg Mensaje que se mostrará en la excepción
     */
    public UsuarioIncorrectoExcepcion(String msg) {
        super(msg);
    }

}
